document.getElementById('findIpButton').addEventListener('click', async () => {
    const infoDiv = document.getElementById('info');
    infoDiv.innerHTML = 'Завантаження...';

    try {
        // Отримуємо IP адресу клієнта
        const ipResponse = await fetch('https://api.ipify.org/?format=json');
        const ipData = await ipResponse.json();
        const ip = ipData.ip;

        // Отримуємо інформацію про фізичну адресу за IP
        const locationResponse = await fetch(`http://ip-api.com/json/${ip}`);
        const locationData = await locationResponse.json();

        // Виводимо інформацію на екран
        infoDiv.innerHTML = `
            <p>IP адреса: ${ip}</p>
            <p>Континент: ${locationData.continent}</p>
            <p>Країна: ${locationData.country}</p>
            <p>Регіон: ${locationData.regionName}</p>
            <p>Місто: ${locationData.city}</p>
            <p>Район: ${locationData.district}</p>
        `;
    } catch (error) {
        infoDiv.innerHTML = 'Сталася помилка при отриманні інформації.';
        console.error('Error:', error);
    }
});
